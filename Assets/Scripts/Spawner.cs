﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject[] obsticalPatterns;

    private float timeBtSpawn;
    public float startTimeBtSpawn;
    public float decreesTime;
    public float minTime = 0.65f;
    void Update()
    {
        if (timeBtSpawn <= 0) {
            int rang = Random.Range(0,3);
            Instantiate(obsticalPatterns[rang], transform.position, Quaternion.identity);
            timeBtSpawn = startTimeBtSpawn;
            if (startTimeBtSpawn < minTime) {
                startTimeBtSpawn -= decreesTime;
            }
        } else {
            timeBtSpawn -= Time.deltaTime;
        } 
    }
}
